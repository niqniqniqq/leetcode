<?php

class Solution
{

    /**
     *
     * @param String[] $words
     * @return String
     */
    function longestWord($words)
    {
        $dSortWordList = [];
        foreach ($words as $word) {
            $dSortWordList[] = $word;
        }
        // usort($dSortWordList, function($a, $b) { return strlen($a) <= strlen($b); });
        usort($dSortWordList, [$this, 'descAndDicSort']);

        foreach ($dSortWordList as $word) {
            $isSuitable = true;
            for ($i = 1; $i < strlen($word); $i ++) {
                $sub = substr($word, 0, $i);
                if (! in_array($sub, $words)) {
                    $isSuitable = false;
                    break;
                }
            }
            if ($isSuitable) {
                return $word;
            }
        }
        return '';
    }

    function descAndDicSort($a, $b)
    {
        $alen = strlen($a);
        $blen = strlen($b);
        if ($alen == $blen) {
            return strnatcmp($a, $b);
        }
        return $alen < $blen;
    }
}
?>