<?php

class Solution
{

    /**
     *
     * @param String $date
     * @return Integer
     */
    function dayOfYear($date)
    {
        $splitDate = explode('-', $date);
        if (count($splitDate) != 3) {
            return null;
        }

        $year = $splitDate[0];
        $month = $splitDate[1];
        $date = $splitDate[2];
        $returnDate = 0;

        for ($i = 1; $i <= $month - 1; $i ++) {
            $lastDate = date('d', strtotime('last day of ' . $year . '-' . $i));
            $returnDate += $lastDate;
        }
        return $returnDate += $date;
    }
}
?>