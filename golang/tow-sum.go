//初回コード
//func twoSum(nums []int, target int) []int {
//	var ret []int
//	for i, v := range nums {
//		diff := target - v
//		if j := inArray(i, diff, nums); j != -1 {
//			ret = append(ret, i)
//			ret = append(ret, j)
//			break
//		}
//	}
//	return ret
//}
//
//func inArray(notIndex int, target int, nums []int) int {
//	for i, v := range nums {
//		if i == notIndex {
//			continue
//		}
//		if target == v {
//			return i
//		}
//	}
//	return -1
//}

// 学習コード
func twoSum(nums []int, target int) []int {
	m := map[int]int{}
	for i, v := range nums {
		m[v] = i
	}
	var ret []int
	for i, v := range nums {
		diff := target - v
		if mi, mv := m[diff]; mv && mi != i {
			ret = append(ret, i)
			ret = append(ret, mi)
			break
		}
	}
	return ret
}
