func searchInsert(nums []int, target int) int {
	p := doBinarySearch(nums, target)
	return p
}

func doBinarySearch(nums []int, target int) int {
	left := 0
	right := len(nums) - 1
	for left <= right {
		p := left + (right-left)/2

		if target == nums[p] {
			return p
		}
		if nums[p] < target {
			left = p + 1 //探す値より小さい場合、左側にはもっと小さい値しかないので左端の値を真ん中の値の右に移動する
		} else {
			right = p - 1 // 探す値より大きい場合、右側にはもっと大きい値しかないので右端の値を真ん中の値の左に移動する
		}
	}
	return left
}