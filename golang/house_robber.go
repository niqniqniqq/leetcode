var mem []int

func rob(nums []int) int {
	mem = make([]int, len(nums))
	for i, _ := range nums {
		mem[i] = -1
	}
	return getMany(nums, len(nums)-1)
}

func getMany(nums []int, h int) int {
	if h < 0 {
		return 0
	}
	if mem[h] >= 0 {
		return mem[h]
	}
	result := max(getMany(nums, h-2)+nums[h], getMany(nums, h-1))
	mem[h] = result
	return result
}

func max(a, b int) int {
	if a > b {
		return a
	}
	return b
}