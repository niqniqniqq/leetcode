	// 1 2 4 5
	// min = 1, max = 5
	// Increment 5 - 1 = 4 to all elements expect max
	// moves = 4

	// 1 2 4 5
	// 5 5 6 8
	// min = 5, max = 8
	// increment 8 - 5 = 3 to all elements expect max
	// moves = 4 + 3 = 7

	// 8 8 8 9
	// min = 8, max = 9
	// increment 9 - 8 = 1 to all elements expect max
	// moves = 7 + 1 = 8

	// 9 9 9 9
	// min = max = 9
	// terminate
	// moves = 8

	// Now, switch to a better way to find the pattern.

	// #1 original
	//    x
	//  x x
	//  x x x
	//x x x x

	// #2 Add 4-1=3 to each col other than 4 items
	//  A
	//  A   A
	//A A x A
	//A x x A
	//A x x x
	//x x x x

	// #3 Add 6-4=2 to each col other than 6 items
	//      C
	//C A C C
	//C A C A
	//A A x A
	//A x x A
	//A x x x
	//x x x x

	// # 4 Add 7-6=1 to each col other than 7 items
	//E E E C
	//C A C C
	//C A C A
	//A A x A
	//A x x A
	//A x x x
	//x x x x

	// After #4, we have each col same hight.
	// Extra items added on top of col 1 is our answer.
	// Observing the pattern, each cols other than 1 contribute the answer
	// by difference of col size compared with col 1.

	func minMoves(nums []int) int {
		min := findMin(nums)
		sum := 0
		for _, v := range nums {
			sum += v - min
		}
		return sum
	}

	func findMin(nums []int) int {
		min := nums[0]

		for _, v := range nums {
			if v < min {
				min = v
			}
		}
		return min
	}