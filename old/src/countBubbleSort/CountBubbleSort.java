package countBubbleSort;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

public class CountBubbleSort {

	/** ファイルパスリスト */
	private static List<String> filePathList = new LinkedList<String>();
	static {
		filePathList.add("src/countBubbleSort/case01.in.txt");
		filePathList.add("src/countBubbleSort/case02.in.txt");
		filePathList.add("src/countBubbleSort/case03.in.txt");
		filePathList.add("src/countBubbleSort/case04.in.txt");
		filePathList.add("src/countBubbleSort/case05.in.txt");
	}

	public static void main(String[] arg) throws IOException {
		CountBubbleSort cBs = new CountBubbleSort();
		String line = null;
		BufferedReader br = null;
		int sortCnt = -1;
		for (String path : filePathList) {
			System.out.println("【読み込みファイル】" + path.substring(path.lastIndexOf("/") + 1));
			br = cBs.getFileReader(path);
			while (null != (line = br.readLine())) {
				if (line.isEmpty()) {
					continue;
				}
				sortCnt = cBs.countBubbleSort(cBs.convertStrToArray(line));
				System.out.print(sortCnt + ",");
			}
			System.out.println();
		}
	}

	/**
	 * 指定されたファイルのReaderを取得する.
	 *
	 * @param path ファイルパス＋ファイル名
	 * @return ファイル内容
	 */
	private BufferedReader getFileReader(String path) {
		BufferedReader br = null;
		InputStreamReader isr = null;
		try {
			isr = new InputStreamReader(new FileInputStream(path));
			br = new BufferedReader(isr);
		} catch (IOException e) {
			System.out.println(e);
		}
		return br;
	}

	/**
	 * 文字列を配列へ変換
	 *
	 * @param str 文字列
	 * @return 文字列配列
	 */
	private Integer[] convertStrToArray(String str) {
		List<Integer> tmpList = new ArrayList<Integer>();
		String[] strSplit = str.split("", -1);
		for (int i = 1; i < strSplit.length - 1; i++) {
			tmpList.add(Integer.valueOf(strSplit[i]));
		}
		return tmpList.toArray(new Integer[0]);
	}

	/**
	 * 指定された int 配列の反転数(交換回数)を返します
	 *
	 * @param values 配列
	 * @return 反転数(交換回数)
	 */
	private int countBubbleSort(Integer[] values) {
		int cnt = 0;
		if (values == null || values.length == 0) {
			return cnt;
		}
		for (int i = 0; i < values.length - 1; i++) {
			for (int j = i + 1; j < values.length; j++) {
				if (values[i] > values[j]) {
					cnt++;
				}
			}
		}
		return cnt;
	}
}
