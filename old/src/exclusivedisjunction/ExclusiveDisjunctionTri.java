package exclusivedisjunction;

import java.util.LinkedList;
import java.util.List;

public class ExclusiveDisjunctionTri {

	/** 0出現回数上限 */
	public static int zeroMax = 24;

	public static void main(String[] arg) {
		boolean breakKey = false;
		int ed = -1;
		int zeroCunt = 0;
		// 処理中行数カウント
		int rowCnt = 0;
		// 最大列カウント
		int colMaxCont = 0;
		List<List<Integer>> rowColList = new LinkedList<List<Integer>>();
		while (!breakKey) {
			List<Integer> workList = new LinkedList<Integer>();
			for (int j = 0; j <= colMaxCont; j++) {
				if (j == rowCnt) {
					// 右端到達のためループから抜ける
					workList.add(1);
					break;
				} else if (j == 0) {
					// *行の左端は1
					workList.add(1);
				} else {
					// 左右端以外の途中は一つ上の二つの要素の排他的論理和
					ed =
						getExclusiveDisjunction(rowColList.get(rowCnt - 1).get(j - 1), rowColList.get(rowCnt - 1)
							.get(j));
					workList.add(ed);
					if (0 == ed) {
						// 0出現回数加算
						zeroCunt++;
					}
				}
			}
			rowColList.add(workList);
			// 0が指定回数以上出ていたらループから抜ける
			if (zeroMax <= zeroCunt) {
				breakKey = true;
			} else {
				rowCnt++;
				colMaxCont++;
			}
		}
		// 結果表示
		for (List<Integer> rowlist : rowColList) {
			for (Integer value : rowlist) {
				System.out.print(value);
			}
			System.out.println();
		}
		System.out.println(zeroMax + "個目の0は" + (rowCnt + 1) + "行目に存在します");
	}

	/**
	 * 排他的論理和を取得
	 *
	 * @param i 要素１
	 * @param j 要素２
	 * @return 排他的論理和
	 */
	private static int getExclusiveDisjunction(int i, int j) {
		if (1 == i && 1 == j) {
			return 0;
		} else if (1 == i || 1 == j) {
			return 1;
		} else {
			return 0;
		}
	}
}
