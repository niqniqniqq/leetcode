package binaryToDecimal;

public class CharOneCounter {

	/** 作業文字列 */
	private static StringBuilder sb = null;

	public CharOneCounter() {
		sb = new StringBuilder();
	}

	/**
	 * メイン
	 *
	 * @param arg 第一引数に数値を指定
	 */
	public static void main(String[] arg) {
		CharOneCounter cOc = new CharOneCounter();
		int existCnt = 0;
		for (int i = 10; i < 100; i++) {
			String binary = Integer.toBinaryString(i);
			String bcd = cOc.decimalToBinaryCodedDecimal(i);
			int binaryCnt = cOc.cntOneInString(binary);
			int bcdCnt = cOc.cntOneInString(bcd);
			if (binaryCnt == bcdCnt) {
				existCnt++;
			}
			// System.out.println("【2進数】" + binary + "【１の個数】" + binaryCnt);
			// System.out.println("【2進化10進数】" + bcd + "【１の個数】" + bcdCnt);
		}
		System.out.println("【答え】" + existCnt + "個");
	}

	/**
	 * 10進数を2進化10進数へ変換
	 *
	 * @param str 文字列
	 * @return 2進化10進数
	 */
	public String decimalToBinaryCodedDecimal(int decimal) {
		sb.setLength(0);
		String[] strArray = String.valueOf(decimal).split("", -1);
		for (int i = 1; i < strArray.length - 1; i++) {
			sb.append(Integer.toBinaryString(Integer.valueOf(strArray[i])));
		}
		return sb.toString();
	}

	/**
	 * 文字列中の1の数をカウントします
	 *
	 * @param str 文字列
	 * @return 1の数
	 */
	public int cntOneInString(String str) {
		int cnt = 0;
		String[] array = str.split("", -1);
		for (String arraySplit : array) {
			if (arraySplit.equals("1")) {
				cnt++;
			}
		}
		return cnt;
	}
}
