package dethcoro;

public class Ideone {

	public static void main(String[] args) throws java.lang.Exception {
		char X = 97;
		for (int i = 0; i <= 24; i++) {
			for (int j = 0; j <= 24; j++) {
				System.out.print(i == j ? (char) (X + (i + 1)) : X);
			}
			System.out.println();
		}
	}
}
