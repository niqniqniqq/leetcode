/*
 * 作成者 y.ohsawa
 * 作成日 2013/05/15
 * 更新履歴 更新日 担当者 内容
 * 01.00.00 2013/05/15 y.ohsawa 新規作成
 */
import java.io.UnsupportedEncodingException;
import java.math.BigInteger;
import java.net.URLEncoder;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.text.DecimalFormat;

import java.util.Arrays;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.ibm.icu.text.Transliterator;

/**
 * <pre>
 * 文字列に関する処理を定義したクラス.
 * </pre>
 *
 * @author y.ohsawa
 * @version 01.00.00
 */
public class StringUtil {

	/** 降順ソートする為のコンパレータ */
	private static Comparator<String> DESC_COMPARATOR = new Comparator<String>() {

		@Override
		public int compare(String o1, String o2) {
			return o2.compareTo(o1);
		}
	};

	/** 標準文字コード */
	private static final String DEFAULT_CHAR_CODE = "UTF-8";

	/** BOM */
	private static final byte[] BOM = new byte[] { (byte) 0xEF, (byte) 0xBB, (byte) 0xBF };

	/**
	 * エスケープ文字マップ
	 */
	private static Map<Character, String> ESCAPE_MAP = new HashMap<Character, String>();
	static {
		ESCAPE_MAP.put('"', "\\\"");
		ESCAPE_MAP.put('$', "\\$");
		ESCAPE_MAP.put('&', "\\&amp;");
		ESCAPE_MAP.put('\'', "\\&apos;");
		ESCAPE_MAP.put('(', "\\(");
		ESCAPE_MAP.put(')', "\\)");
		ESCAPE_MAP.put('*', "\\*");
		ESCAPE_MAP.put('+', "\\+");
		ESCAPE_MAP.put(',', "\\,");
		ESCAPE_MAP.put('-', "\\-");
		ESCAPE_MAP.put('.', "\\.");
		ESCAPE_MAP.put('?', "\\?");
		ESCAPE_MAP.put('[', "\\[");
		ESCAPE_MAP.put('\\', "\\\\");
		ESCAPE_MAP.put(']', "\\]");
		ESCAPE_MAP.put('^', "\\^");
		ESCAPE_MAP.put('{', "\\{");
		ESCAPE_MAP.put('|', "\\|");
		ESCAPE_MAP.put('}', "\\}");
		ESCAPE_MAP.put('~', "\\~");
		ESCAPE_MAP.put('<', "&lt;");
		ESCAPE_MAP.put('>', "&gt;");
	}

	/**
	 * 文字変換(半角カナ→全角カナ)
	 */
	private static Transliterator HANKAKU_ZENKAKU_TRANS = Transliterator.getInstance("Halfwidth-Fullwidth");

	/**
	 * 文字変換(全角カナ→半角カナ)
	 */
	private static Transliterator ZENKAKU_HANKAKU_TRANS = Transliterator.getInstance("Fullwidth-Halfwidth");

	/** 数値型フォーマット（小数以下なし） */
	private static DecimalFormat NUMBER_FORMAT = new DecimalFormat("###,###");

	/** メッセージダイジェスト */
	private static MessageDigest md = null;

	/**
	 * コンストラクタ
	 */
	private StringUtil() {
		super();
	}

	/**
	 * 文字列から、改行、タブ、両端ブランクを削除する
	 *
	 * @param s
	 *            文字列
	 * @return 編集後文字列
	 */
	public static String trimCRLFTab(String s) {
		if (null == s) {
			return "";
		}
		return s.replaceAll("\t", "").replaceAll("\n", "").replaceAll("\r", "").trim();
	}

	/**
	 * カンマ区切りの数値型に変換します.
	 *
	 * @param s
	 *            文字列（数値）
	 * @return フォーマット後文字列
	 */
	public static String formatNumber(long s) {
		return NUMBER_FORMAT.format(s);
	}

	/**
	 * ストリング配列を単純文字列にします.
	 *
	 * @param <T>
	 *            ストリング配列
	 * @param itemArray
	 *            ストリング配列
	 * @return 単純文字列
	 */
	public static <T extends Object> String arrayToString(T... itemArray) {
		StringBuilder sb = new StringBuilder();
		for (T o : itemArray) {
			sb.append(o);
		}
		return sb.toString();
	}

	/**
	 * ストリング配列をカンマ区切りの文字列にします.
	 *
	 * @param <T>
	 *            ストリング配列
	 * @param itemArray
	 *            ストリング配列
	 * @return 単純文字列
	 */
	public static <T extends Object> String arrayToStringUseDelimiter(T... itemArray) {
		StringBuilder sb = new StringBuilder();
		for (T o : itemArray) {
			sb.append(o);
			sb.append(",");
		}
		return sb.toString();
	}

	/**
	 * 文字変換(半角カナ→全角カナ)
	 *
	 * @param text
	 *            変換前文字列
	 * @return 変換文字列
	 */
	public static String convertHankakuToZenkaku(String text) {
		return HANKAKU_ZENKAKU_TRANS.transliterate(text);
	}

	/**
	 * 文字変換(全角カナ→半角カナ)
	 *
	 * @param text
	 *            変換前文字列
	 * @return 変換文字列
	 */
	public static String convertZenkakuToHankaku(String text) {
		return ZENKAKU_HANKAKU_TRANS.transliterate(text);
	}

	/**
	 * 文字変換(URLエンコード)
	 *
	 * @param text
	 *            変換前文字列
	 * @return 変換文字列
	 * @throws UnsupportedEncodingException
	 */
	public static String convertUrlEncode(String text) throws UnsupportedEncodingException {
		return convertUrlEncode(text, DEFAULT_CHAR_CODE);
	}

	/**
	 * 文字変換(URLエンコード)
	 *
	 * @param text
	 *            変換前文字列
	 * @param encode
	 *            エンコード
	 * @return 変換文字列
	 * @throws UnsupportedEncodingException
	 */
	public static String convertUrlEncode(String text, String encode) throws UnsupportedEncodingException {
		return URLEncoder.encode(text, encode);
	}

	/**
	 * 例外文字変換
	 *
	 * @param text
	 *            変換前文字列
	 * @param encode
	 *            ファイルエンコード
	 * @return 変換文字列
	 * @throws UnsupportedEncodingException
	 */
	public static String convertLowValueToSpace(String text, String encode) throws UnsupportedEncodingException {
		StringBuilder sb = new StringBuilder();
		for (int i = 0; i < text.length(); i++) {
			String s = text.substring(i, i + 1);
			byte[] bytes = s.getBytes(encode);
			if (bytes.length == 1) {
				if ((bytes[0] & 0xFF) == 0x00) {
					s = " ";
				}
			}
			sb.append(s);
		}
		return sb.toString();
	}

	/**
	 * セパレーター変換
	 *
	 * @param texts
	 *            変換前文字配列
	 * @param separator
	 *            区切り文字
	 * @param enclose
	 *            項目くくり
	 * @return 変換文字列
	 */
	public static String convertSeparator(List<String> texts, String separator, String enclose) {
		return convertSeparator(texts, separator, enclose, null);
	}

	/**
	 * セパレーター変換
	 *
	 * @param texts
	 *            変換前文字配列
	 * @param separator
	 *            区切り文字
	 * @param enclose
	 *            項目くくり
	 * @param encode
	 *            エンコード
	 * @return 変換文字列
	 */
	public static String convertSeparator(List<String> texts, String separator, String enclose, String encode) {
		StringBuilder sb = new StringBuilder();
		for (String s : texts) {
			sb.append(separator);
			sb.append(enclose);
			sb.append(convertTextEncode(s, encode));
			sb.append(enclose);
		}
		return sb.toString().replaceFirst(separator, "");
	}

	/**
	 * セパレーター変換
	 *
	 * @param texts
	 *            変換前文字配列
	 * @param separator
	 *            区切り文字
	 * @param enclose
	 *            項目くくり
	 * @return 変換文字列
	 */
	public static String convertSeparator(String[] texts, String separator, String enclose) {
		return convertSeparator(texts, separator, enclose, null);
	}

	/**
	 * セパレーター変換
	 *
	 * @param texts
	 *            変換前文字配列
	 * @param separator
	 *            区切り文字
	 * @param enclose
	 *            項目くくり
	 * @param encode
	 *            エンコード
	 * @return 変換文字列
	 */
	public static String convertSeparator(String[] texts, String separator, String enclose, String encode) {
		StringBuilder sb = new StringBuilder();
		for (String s : texts) {
			sb.append(separator);
			sb.append(enclose);
			sb.append(convertTextEncode(s, encode));
			sb.append(enclose);
		}
		return sb.toString().replaceFirst(separator, "");
	}

	/**
	 * 文字列をエンコードする
	 *
	 * @param text
	 *            文字列
	 * @param encode
	 *            エンコード
	 * @return 変換後文字列
	 */
	public static String convertTextEncode(String text, String encode) {
		if (null == text || text.isEmpty()) {
			return "";
		}
		if (null == encode || encode.isEmpty()) {
			return text;
		}
		try {
			return new String(text.getBytes(encode), encode);
		} catch (UnsupportedEncodingException e) {
			return text;
		}
	}

	/**
	 * パスの最後にセパレータを付加する.
	 *
	 * @param path
	 *            パス
	 * @return セパレータ付加後パス
	 */
	public static String addFileSeparator(String path) {
		String newPath = path;
		if (null == newPath || newPath.isEmpty()) {
			newPath = "";
		} else {
			if (!newPath.substring(newPath.length() - 1).equals("/")) {
				newPath += "/";
			}
		}
		return newPath;
	}

	/**
	 * 拡張子の削除を行う.
	 *
	 * @param fileName
	 *            ファイル名
	 * @return 変換後ファイル名
	 */
	public static String deleteExtension(String fileName) {
		String newFileName = fileName;
		if (newFileName.indexOf('.') > 0) {
			newFileName = newFileName.substring(0, newFileName.lastIndexOf('.'));
		}
		return newFileName;
	}

	/**
	 * 全拡張子の削除を行う.
	 *
	 * @param fileName
	 *            ファイル名
	 * @return 変換後ファイル名
	 */
	public static String deleteAllExtension(String fileName) {
		String newFileName = fileName;
		while (newFileName.indexOf('.') > 0) {
			newFileName = newFileName.substring(0, newFileName.lastIndexOf('.'));
		}
		return newFileName;
	}

	/**
	 * 拡張子の変更を行う.
	 *
	 * @param fileName
	 *            ファイル名
	 * @param replaceExtension
	 *            拡張子
	 * @return 変換後ファイル名
	 */
	public static String convertExtension(String fileName, String replaceExtension) {
		String newFileName = fileName;
		if (null != replaceExtension && !replaceExtension.isEmpty()) {
			if (newFileName.indexOf('.') > 0) {
				newFileName = newFileName.substring(0, newFileName.lastIndexOf('.'));
			}
			newFileName += "." + replaceExtension;
		}
		return newFileName;
	}

	/**
	 * BOMを削除する.
	 *
	 * @param line
	 *            データ
	 * @return BOM削除後データ
	 */
	public static byte[] deleteBOM(String line) {
		if (null == line || line.isEmpty()) {
			return new byte[0];
		}
		byte[] byteLine = line.getBytes();
		if (byteLine.length < 3) {
			return byteLine;
		}
		if (byteLine[0] != BOM[0] || byteLine[1] != BOM[1] || byteLine[2] != BOM[2]) {
			return byteLine;
		}
		return Arrays.copyOfRange(byteLine, 3, byteLine.length);
	}

	/**
	 * 変換文字定義による文字の置き換えを行う.
	 *
	 * @param sb
	 *            文字列作業域
	 * @param map
	 *            変換マップ
	 * @param item
	 *            項目値
	 * @return 変換後文字列
	 */
	public static String convertReplaceChar(StringBuilder sb, Map<String, String> map, String item) {
		if (null == map || map.isEmpty()) {
			return item;
		}
		sb.setLength(0);
		for (int i = 0; i < item.length(); i++) {
			String charText = item.substring(i, i + 1);
			byte[] byteText = charText.getBytes();
			if (1 == byteText.length) {
				String replaceText = map.get(String.format("%02x", byteText[0]).toUpperCase());
				if (null != replaceText) {
					charText = replaceText;
				}
			}
			sb.append(charText);
		}
		return sb.toString();
	}

	/**
	 * 変換文字定義による文字の置き換えを行う.
	 *
	 * @param map
	 *            変換マップ
	 * @param item
	 *            項目値
	 * @return 変換後文字列
	 */
	public static String convertReplaceChar(Map<String, String> map, String item) {
		return convertReplaceChar(new StringBuilder(), map, item);
	}

	public static String replaceText(String delimiter, String text, String str, String repStr) {
		String tmpStr = "";
		String[] textSplit = text.split(delimiter, -1);
		for (int i = 0; i < textSplit.length; i++) {
			if (i != 0) {
				tmpStr += ",";
			}
			if (textSplit[i].equals(str)) {
				tmpStr += textSplit[i].replace(str, repStr);
			} else {
				tmpStr += textSplit[i];
			}
		}
		return tmpStr;
	}

	/**
	 * MD5のHash値を作成する.
	 *
	 * @param value
	 *            作成したい文字列
	 * @return Hash値
	 * @throws NoSuchAlgorithmException
	 */
	public static String createHashValue(String value) throws NoSuchAlgorithmException {
		if (null == md) {
			md = MessageDigest.getInstance("MD5");
		}
		// MD5は必ず16バイトが帰る
		md.update(value.getBytes());
		byte[] digest = md.digest();
		byte[] dest = new byte[digest.length + 1];
		System.arraycopy(digest, 0, dest, 1, digest.length);
		return new BigInteger(dest).toString();
	}

	/**
	 * 要素を昇順にソートする.
	 *
	 * @param values
	 *            要素
	 */
	public static void ascSort(String[] values) {
		Arrays.sort(values);
	}

	/**
	 * 要素を降順にソートする.
	 *
	 * @param values
	 *            要素
	 */
	public static void descSort(String[] values) {
		Arrays.sort(values, DESC_COMPARATOR);
	}
}
