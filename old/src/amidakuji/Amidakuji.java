package amidakuji;

import java.util.LinkedList;
import java.util.List;

import numberplace.FileUtil;

public class Amidakuji {

	/** おみくじ2次元配列化フィールド */
	private String[][] omikujiFld = null;

	/** おみくじ2次元配列化フィールド行数 */
	private int rowSize = -1;

	/** おみくじ2次元配列化フィールドカラム数 */
	private int colSize = -1;

	public static void main(String[] arg) {
		Amidakuji amdkj = new Amidakuji();
		amdkj.initOmikuji(FileUtil.getFileContents("src/amidakuji/sample1.csv"));
		amdkj.getAnswer();
	}

	/**
	 * 必要パラメータを初期化します
	 *
	 * @param list 入力ファイル内容
	 */
	private void initOmikuji(List<String> list) {
		List<String> tmpList = new LinkedList<String>();
		List<String[]> workList = new LinkedList<String[]>();
		String[] lineSplit = null;
		int rowCnt = 0;
		for (String line : list) {
			lineSplit = line.split(",", -1);
			tmpList.clear();
			tmpList.add("■");
			for (int i = 0; i < lineSplit.length; i++) {
				if (lineSplit[i].equals("0")) {
					tmpList.add("□");
				}
				if (lineSplit[i].equals("1")) {
					tmpList.add("■");
				}
				tmpList.add("■");
			}
			workList.add(tmpList.toArray(new String[0]));
		}
		colSize = workList.get(0).length;
		rowSize = workList.size();
		omikujiFld = new String[rowSize][colSize];
		rowCnt = 0;
		for (String[] line : workList) {
			for (int i = 0; i < line.length; i++) {
				omikujiFld[rowCnt][i] = line[i];
			}
			rowCnt++;
		}
	}

	/**
	 * 答えを表示します
	 */
	private void getAnswer() {
		for (int i = 0; i <= colSize; i = i + 2) {
			selecJtCondition(i, 0, i, 0);
		}
	}

	/**
	 * おみくじを実行します
	 * (結果確定まで再帰的に呼び出されます)
	 *
	 * @param preX 前回位置
	 * @param preY 前回位置
	 * @param nowX 今回位置
	 * @param nowY 今回位置
	 */
	private void selecJtCondition(int preX, int preY, int nowX, int nowY) {
		// 横移動判定が優先
		if (preX > nowX) {
			// 右から左へ移動してきた場合
			if (0 != nowX) {
				// 現在位置が左端ではない場合、左に移動可能か確認
				if (omikujiFld[nowY][nowX - 1].equals("■")) {
					selecJtCondition(nowX, nowY, nowX - 1, nowY);
					return;
				}
			}
		} else if (nowX > preX) {
			// 左から右へ移動してきた場合
			if (colSize - 1 != nowX) {
				// 現在位置が右端ではない場合、右に移動可能か確認
				if (omikujiFld[nowY][nowX + 1].equals("■")) {
					selecJtCondition(nowX, nowY, nowX + 1, nowY);
					return;
				}
			}
		} else if (preX == nowX) {
			// 初期位置 or 下移動後の場合
			if (0 == nowX) {
				// 現在値左端の場合
				if (omikujiFld[nowY][nowX + 1].equals("■")) {
					selecJtCondition(nowX, nowY, nowX + 1, nowY);
					return;
				}
			} else if (colSize - 1 == nowX) {
				// 現在値右端の場合
				if (omikujiFld[nowY][nowX - 1].equals("■")) {
					selecJtCondition(nowX, nowY, nowX - 1, nowY);
					return;
				}
			} else if (omikujiFld[nowY][nowX - 1].equals("■")) {
				selecJtCondition(nowX, nowY, nowX - 1, nowY);
				return;
			} else if (omikujiFld[nowY][nowX + 1].equals("■")) {
				selecJtCondition(nowX, nowY, nowX + 1, nowY);
				return;
			}
		}
		// 下移動判定
		if (rowSize - 1 != nowY) {
			if (omikujiFld[nowY + 1][nowX].equals("■")) {
				selecJtCondition(nowX, nowY, nowX, nowY + 1);
				return;
			}
		}
		// 移動しない場合、移動試行終了
		System.out.println(nowX + 1 - (int) (nowX / 2));
	}
}
