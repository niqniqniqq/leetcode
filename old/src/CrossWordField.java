import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

/**
 * <pre>
 * フィールドを定義したクラス.
 * </pre>
 *
 * @author y.ohsawa
 * @version 01.00.00
 */
public class CrossWordField {

//	/** 基本フィールド */
//	public String[][] field = new String[][] { //
//		{ "■", "２", "□", "■", "■" },//
//			{ "１", "■", "□", "□", "□" }, //
//			{ "□", "□", "□", "■", "■" },//
//			{ "４", "■", "□", "□", "■" }, //
//			{ "■", "□", "□", "３", "□" } };

	// /** テストフィールド１ */
	// public String[][] field = new String[][] { //
	// { "□", "■", "□", "□", "■", "□", "□" },//
	// { "□", "３", "□", "□", "□", "□", "■" },//
	// { "□", "□", "■", "□", "■", "□", "□" },//
	// { "■", "□", "□", "５", "□", "■", "□" },//
	// { "□", "□", "□", "■", "４", "□", "□" },//
	// { "１", "■", "□", "□", "□", "■", "□" },//
	// { "□", "□", "□", "■", "□", "□", "２" } };
	 /** テストフィールド２ */
	 public String[][] field = new String[][] { //
	 { "□", "□", "□", "■", "□", "□", "□", "■", "□", "□", "□" },//
	 { "□", "□", "■", "□", "■", "□", "□", "□", "□", "■", "□" },//
	 { "□", "□", "□", "□", "□", "■", "□", "■", "□", "□", "□" },//
	 { "□", "□", "□", "■", "□", "□", "□", "□", "■", "□", "□" },//
	 { "□", "■", "□", "□", "□", "□", "■", "□", "□", "□", "□" },//
	 { "□", "□", "□", "■", "□", "□", "□", "□", "■", "□", "■" },//
	 { "■", "□", "□", "□", "■", "□", "□", "□", "□", "□", "□" },//
	 { "□", "□", "■", "□", "□", "■", "□", "■", "□", "□", "□" },//
	 { "□", "□", "□", "■", "□", "□", "□", "□", "■", "□", "□" },//
	 { "□", "■", "□", "□", "□", "□", "□", "■", "□", "□", "□" },//
	 { "□", "□", "□", "■", "□", "□", "■", "□", "□", "■", "□" } };
	/** 解答用フィールド */
	private String[][] answerField;

	/** 答え位置 */
	private Map<Integer[], String> answerMap = new HashMap<Integer[], String>();

	/** 行長さ */
	private int FldRowsSize;

	/** 列長さ */
	private int FldColsSize;

	/** 作業リスト */
	private List<String> workList = new ArrayList<String>();

	/**
	 * コンストラクタ
	 */
	public CrossWordField() {
		FldRowsSize = field.length;
		FldColsSize = field[0].length;
		initPram();
		setNoToEmptyColmun();
	}

	/**
	 * 初期化
	 */
	public void initPram() {
		answerField = new String[FldRowsSize][FldColsSize];
		for (int i = 0; i < FldRowsSize; i++) {
			for (int j = 0; j < FldColsSize; j++) {
				String fieldValue = field[i][j];
				if (CrossWordEnum.INVILED != CrossWordEnum.getEnumByName(fieldValue)) {
					answerField[i][j] = CrossWordEnum.EMPTY.getName();
					if (CrossWordEnum.EMPTY != CrossWordEnum.getEnumByName(fieldValue)) {
						answerMap.put(new Integer[] { i, j }, StringUtil.convertZenkakuToHankaku(fieldValue));
					}
				} else {
					answerField[i][j] = CrossWordEnum.INVILED.getName();
				}
			}
		}
	}

	/**
	 * フィールド文字入力カラムに項番する
	 */
	private void setNoToEmptyColmun() {
		int hCnt = 1;
		int vCnt = 1;
		String tmpValue = null;
		for (int i = 0; i < FldRowsSize; i++) {
			for (int j = 0; j < FldColsSize; j++) {
				if (0 == j) {
					tmpValue = answerField[i][j];
				} else {
					if (CrossWordEnum.EMPTY == CrossWordEnum.getEnumByName(answerField[i][j])
						&& answerField[i][j].equals(tmpValue)) {
						answerField[i][j - 1] = "h" + String.valueOf(hCnt++);
						answerField[i][j] = "h" + String.valueOf(hCnt++);
					} else if (CrossWordEnum.EMPTY == CrossWordEnum.getEnumByName(answerField[i][j])
						&& CrossWordEnum.INVILED != CrossWordEnum.getEnumByName(tmpValue)
						&& CrossWordEnum.EMPTY != CrossWordEnum.getEnumByName(tmpValue)) {
						answerField[i][j] = "h" + String.valueOf(hCnt++);
					}
					tmpValue = answerField[i][j];
				}
			}
		}
		for (int i = 0; i < FldRowsSize; i++) {
			for (int j = 0; j < FldColsSize; j++) {
				if (0 == j) {
					tmpValue = answerField[j][i];
				} else {
					if (CrossWordEnum.EMPTY == CrossWordEnum.getEnumByName(answerField[j][i])
						&& answerField[j][i].equals(tmpValue)) {
						answerField[j - 1][i] = "v" + String.valueOf(vCnt++);
						answerField[j][i] = "v" + String.valueOf(vCnt++);
					} else if (CrossWordEnum.EMPTY == CrossWordEnum.getEnumByName(answerField[j][i])
						&& CrossWordEnum.INVILED != CrossWordEnum.getEnumByName(tmpValue)
						&& CrossWordEnum.EMPTY != CrossWordEnum.getEnumByName(tmpValue)) {
						answerField[j][i] = "v" + String.valueOf(vCnt++);
					} else if (CrossWordEnum.EMPTY != CrossWordEnum.getEnumByName(answerField[j][i])
						&& CrossWordEnum.INVILED != CrossWordEnum.getEnumByName(answerField[j][i])
						&& CrossWordEnum.EMPTY == CrossWordEnum.getEnumByName(tmpValue)) {
						answerField[j - 1][i] = "v" + String.valueOf(vCnt++);
					}
					tmpValue = answerField[j][i];
				}
			}
		}
	}

	/**
	 * 水平方向文字項番取得
	 *
	 * @return フィールド項番横ワード
	 */
	public List<String> getHwordFiled() {
		workList.clear();
		String tmpValue = "";
		for (int i = 0; i < FldRowsSize; i++) {
			for (int j = 0; j < FldColsSize; j++) {
				if (CrossWordEnum.INVILED == CrossWordEnum.getEnumByName(answerField[i][j])) {
					if (0 != j && !tmpValue.isEmpty() && 1 < tmpValue.replaceAll("\\d", "").length()) {
						workList.add(tmpValue);
					}
					tmpValue = "";
					continue;
				}
				if (!tmpValue.isEmpty()) {
					tmpValue += ",";
				}
				tmpValue += answerField[i][j];
			}
			if (!tmpValue.isEmpty() && 1 < tmpValue.replaceAll("\\d", "").length()) {
				workList.add(tmpValue);
			}
			tmpValue = "";
		}
		return workList;
	}

	/**
	 * 垂直方向文字項番取得
	 *
	 * @return フィールド項番縦ワード
	 */
	public List<String> getVwordFiled() {
		workList.clear();
		String tmpValue = "";
		for (int i = 0; i < FldRowsSize; i++) {
			for (int j = 0; j < FldColsSize; j++) {
				if (CrossWordEnum.INVILED == CrossWordEnum.getEnumByName(answerField[j][i])) {
					if (0 != j && !tmpValue.isEmpty() && 1 < tmpValue.replaceAll("\\d", "").length()) {
						workList.add(tmpValue);
					}
					tmpValue = "";
					continue;
				}
				if (!tmpValue.isEmpty()) {
					tmpValue += ",";
				}
				tmpValue += answerField[j][i];
			}
			if (!tmpValue.isEmpty() && 1 < tmpValue.replaceAll("\\d", "").length()) {
				workList.add(tmpValue);
			}
			tmpValue = "";
		}
		return workList;
	}

	/**
	 * 答え項番ワードを取得
	 *
	 * @return 答え項番ワード
	 */
	public String getAnswerWord() {
		StringBuilder sb = new StringBuilder();
		List<Map.Entry<Integer[], String>> entries =
			new ArrayList<Map.Entry<Integer[], String>>(answerMap.entrySet());
		Collections.sort(entries, new Comparator<Map.Entry<Integer[], String>>() {

			@Override
			public int compare(Entry<Integer[], String> entry1, Entry<Integer[], String> entry2) {
				return entry1.getValue().compareTo(entry2.getValue());
			}
		});
		for (Entry<Integer[], String> s : entries) {
			sb.append(answerField[s.getKey()[0]][s.getKey()[1]]);
			sb.append(",");
		}
		return sb.toString();
	}

	/**
	 * 答え項番フィールドを取得
	 *
	 * @param map
	 * @return 答え項番フィールド
	 */
	public String[][] getAnswerFiled(Map<String, String> map) {
		String[][] answer = new String[FldRowsSize][FldColsSize];
		for (int i = 0; i < FldRowsSize; i++) {
			for (int j = 0; j < FldRowsSize; j++) {
				for (Map.Entry<String, String> e : map.entrySet()) {
					if (answerField[i][j].equals(e.getKey())) {
						answer[i][j] = e.getValue();
						break;
					} else {
						answer[i][j] = answerField[i][j];
					}
				}
			}
		}
		return answer;
	}
}
