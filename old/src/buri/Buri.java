package buri;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;

public class Buri {

	/** Sサイズ閾値 */
	private static final int S_SIZE = 95;

	/** Lサイズ閾値 */
	private static final int L_SIZE = 120;

	/** ブリ */
	private static final String BURI = "ブリ";

	/** 作業リスト */
	private static List<Integer> tmpList = new ArrayList<Integer>();

	/** 作業用文字列 */
	private static StringBuilder sb = new StringBuilder();

	/** ファイルパスリスト */
	private static List<String> filePathList = new LinkedList<String>();
	static {
		filePathList.add("src/buri/buri2015.csv");
	}

	/**
	 * メインメソッド
	 *
	 * @param arg コマンド引数
	 */
	public static void main(String[] arg) {
		Buri br = new Buri();
		List<String> fileList = null;
		for (String path : filePathList) {
			fileList = getFileContents(path);
			System.out.println(br.calculateBuriBySizeType(fileList, BURI, "s"));
			System.out.println(br.calculateBuriBySizeType(fileList, BURI, "m"));
			System.out.println(br.calculateBuriBySizeType(fileList, BURI, "l"));
		}
	}

	/**
	 * ファイル内容内の、特定サイズの魚匹数と平均長を返します
	 *
	 * @param list ファイル内容
	 * @param fishName 取得したい魚名(ブリ、スケトウダラ・・・)
	 * @param sizeType 取得するサイズ(s or m or l)
	 * @return 文字列[sizeType(匹数): 平均長cm]
	 */
	private String calculateBuriBySizeType(List<String> list, String fishName, String sizeType) {
		String[] lineSplit = null;
		String fishType = null;
		int fishSize = 0;
		int sum = 0;
		tmpList.clear();
		sb.setLength(0);
		sizeType = sizeType.toLowerCase();
		for (String line : list) {
			lineSplit = line.split(",", -1);
			fishType = lineSplit[0];
			fishSize = Integer.parseInt(lineSplit[1]);
			if (!fishType.equals(fishName)) {
				// ブリ以外は除く
				continue;
			}
			if (sizeType.equals("s")) {
				// Sサイズ
				if (S_SIZE > fishSize) {
					tmpList.add(fishSize);
					continue;
				}
			}
			if (sizeType.equals("l")) {
				// Lサイズ
				if (L_SIZE <= fishSize) {
					tmpList.add(fishSize);
					continue;
				}
			}
			if (sizeType.equals("m")) {
				// Mサイズ
				if (S_SIZE <= fishSize && L_SIZE > fishSize) {
					tmpList.add(fishSize);
					continue;
				}
			}
		}
		// 答え文字列生成
		if (0 == tmpList.size()) {
			return "該当サイズの" + fishName + "は存在しません";
		}
		for (int i : tmpList) {
			sum += i;
		}
		BigDecimal bd = new BigDecimal(sum);
		bd = bd.divide(new BigDecimal(tmpList.size()), 2, BigDecimal.ROUND_HALF_UP);
		sb.append(sizeType.toUpperCase());
		sb.append("(");
		sb.append(tmpList.size());
		sb.append(")");
		sb.append(": ");
		sb.append(bd);
		sb.append("cm");
		return sb.toString();
	}

	/**
	 * 指定された通常ファイル内容をListで取得する.
	 *
	 * @param path ファイルパス＋ファイル名
	 * @param encode エンコード
	 * @return ファイル内容
	 */
	public static List<String> getFileContents(String path) {
		List<String> contentsList = new ArrayList<String>();
		FileInputStream inputStream = null;
		InputStreamReader reader = null;
		BufferedReader br = null;
		try {
			File f = new File(path);
			if (!f.exists() || !f.isFile()) {
				return Collections.emptyList();
			}
			inputStream = new FileInputStream(path);
			reader = new InputStreamReader(inputStream);
			br = new BufferedReader(reader);
			String line = null;
			while (null != (line = br.readLine())) {
				if (line.isEmpty()) {
					continue;
				}
				contentsList.add(line);
			}
		} catch (IOException e) {
			// throw new UserException(e, DHCM000005);
		} finally {
			if (null != inputStream) {
				try {
					inputStream.close();
				} catch (IOException e) {
					// 無視する
				}
			}
			if (null != reader) {
				try {
					reader.close();
				} catch (IOException e) {
					// 無視する
				}
			}
			if (null != br) {
				try {
					br.close();
				} catch (IOException e) {
					// 無視する
				}
			}
		}
		return contentsList;
	}
}
