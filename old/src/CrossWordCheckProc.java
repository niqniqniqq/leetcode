import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * <pre>
 * クロスワード処理クラス.
 * </pre>
 *
 * @author y.ohsawa
 * @version 01.00.00
 */
public class CrossWordCheckProc {

	/** ワードマッチ正規表現 */
	private static final String MATCH_WORD = "^[a-zA-Z0-9]+$";

	/** Fieldクラスの宣言 */
	private CrossWordField cwField = null;

	/** Keyクラスの宣言 */
	private CrossWordKey cwKey = null;

	/** フィールド項番横ワード */
	private List<String> hWordArray = null;

	/** フィールド項番縦ワード */
	private List<String> vWordArray = null;

	/** word要素とkey要素のマップ */
	private Map<String, String> wordElmentKeyElmentMap = new HashMap<String, String>();

	/** 確定ワード-キーマップ */
	private Map<String, String[]> uniqeWordKeyMap = new HashMap<String, String[]>();

	/**
	 * コンストラクタ
	 *
	 * @param cwField Fieldクラスの宣言
	 * @param cwKey Keyクラスの宣言
	 */
	public CrossWordCheckProc(CrossWordField cwField, CrossWordKey cwKey) {
		this.cwField = cwField;
		this.cwKey = cwKey;
		hWordArray = new ArrayList<String>(cwField.getHwordFiled());
		vWordArray = new ArrayList<String>(cwField.getVwordFiled());
	}

	/**
	 * 解答メソッドstep1 word項目に対して、可能性のあるkeyを設定
	 */
	public void check() {
		Map<String[], String[]> hvKeyMap = new HashMap<String[], String[]>();
		List<String> vwordList = new ArrayList<String>();
		Map<String, String[]> tmpWordKeyMap = new HashMap<String, String[]>();
		for (String hword : hWordArray) {
			vwordList = getVWordListbyHWordElem(hword);
			for (String vword : vwordList) {
				String[] index = getHVSameIndex(hword, vword);
				hvKeyMap =
					cwKey.getHVKeyByIndexAndSize(Integer.parseInt(index[0]), hword.split(",", -1).length,
						Integer.parseInt(index[1]), vword.split(",", -1).length);
				for (Map.Entry<String[], String[]> e : hvKeyMap.entrySet()) {
					tmpWordKeyMap.put(hword, editValue(tmpWordKeyMap.get(hword), e.getKey()));
					tmpWordKeyMap.put(vword, editValue(tmpWordKeyMap.get(vword), e.getValue()));
				}
			}
		}
		testDisply("一次マップ", tmpWordKeyMap);
		checkStep2(tmpWordKeyMap);
		displyFiled(cwField.getAnswerFiled(wordElmentKeyElmentMap));
	}

	/**
	 * 解答メソッドstep2 ユニークではないkeyに対して精査
	 */
	private void checkStep2(Map<String, String[]> tmpWordKeyMap) {
		Map<String, String[]> nonUniqeWordKeyMap = new HashMap<String, String[]>();
		for (Map.Entry<String, String[]> e : tmpWordKeyMap.entrySet()) {
			if (1 != e.getValue().length) {
				nonUniqeWordKeyMap.put(e.getKey(), e.getValue());
			} else {
				uniqeWordKeyMap.put(e.getKey(), e.getValue());
			}
		}
		setWorkElKeyElMap(uniqeWordKeyMap);
		if (!nonUniqeWordKeyMap.isEmpty()) {
			testDisply("ユニークマップ", uniqeWordKeyMap);
			testDisply("NONユニークマップ", nonUniqeWordKeyMap);
			checkStep2(reduceNonUniqeWordKeyMap(nonUniqeWordKeyMap, uniqeWordKeyMap));
		}
	}

	/**
	 * 複数候補を減らす
	 *
	 * @param wordKeyMap 複数候補マップ
	 */
	private Map<String, String[]> reduceNonUniqeWordKeyMap(Map<String, String[]> nonUniqeWordKeyMap,
		Map<String, String[]> uniqeWordKeyMap) {
		List<String> wordList = new ArrayList<String>();
		Map<String[], String[]> keyMap = new HashMap<String[], String[]>();
		Map<String, String[]> tmpWordKeyMap = new HashMap<String, String[]>();
		// 1:一意ではないマップからユニーク値を削る
		for (Map.Entry<String, String[]> e : uniqeWordKeyMap.entrySet()) {
			// ユニークマップのため[0]要素確定
			String uniqeKey = e.getValue()[0];
			for (Map.Entry<String, String[]> ne : nonUniqeWordKeyMap.entrySet()) {
				wordList.clear();
				String[] noUniqeKey = ne.getValue();
				for (String nuKeySplit : noUniqeKey) {
					if (!uniqeKey.equals(nuKeySplit)) {
						wordList.add(nuKeySplit);
					}
				}
				nonUniqeWordKeyMap.put(ne.getKey(), wordList.toArray(new String[0]));
			}
		}
		// 2:一意ではないマップキーの妥当性チェック
		wordList.clear();
		for (Map.Entry<String, String[]> e : nonUniqeWordKeyMap.entrySet()) {
			String word = e.getKey();
			if (getVWordListbyHWordElem(word).isEmpty()) {
				wordList = getHWordListbyVWordElem(word);
			} else {
				wordList = getVWordListbyHWordElem(word);
			}
			for (String word2 : wordList) {
				if (uniqeWordKeyMap.containsKey(word2)) {
					continue;
				}
				String editWord = word;
				String editWord2 = word2;
				for (Map.Entry<String, String> weM : wordElmentKeyElmentMap.entrySet()) {
					editWord = StringUtil.replaceText(",", editWord, weM.getKey(), weM.getValue());
					editWord2 = StringUtil.replaceText(",", editWord2, weM.getKey(), weM.getValue());
				}
				String[] index = getHVSameIndex(editWord, editWord2);
				if (null != index) {
					if (hWordArray.contains(word)) {
						keyMap =
							cwKey.getHVKeyByIndexAndSize(Integer.parseInt(index[0]), word.split(",", -1).length,
								Integer.parseInt(index[1]), word2.split(",", -1).length);
						for (Map.Entry<String[], String[]> ke : keyMap.entrySet()) {
							if (uniqeWordKeyMap.containsValue(ke.getKey())
								|| uniqeWordKeyMap.containsValue(ke.getValue())) {
								continue;
							}
							tmpWordKeyMap.put(word, checkStringMach(editWord, ke.getKey()));
							tmpWordKeyMap.put(word2, checkStringMach(editWord2, ke.getValue()));
						}
					} else {
						keyMap =
							cwKey.getHVKeyByIndexAndSize(Integer.parseInt(index[1]), word2.split(",", -1).length,
								Integer.parseInt(index[0]), word.split(",", -1).length);
						for (Map.Entry<String[], String[]> ke : keyMap.entrySet()) {
							if (uniqeWordKeyMap.containsValue(ke.getKey())
								|| uniqeWordKeyMap.containsValue(ke.getValue())) {
								continue;
							}
							tmpWordKeyMap.put(word2, checkStringMach(editWord2, ke.getKey()));
							tmpWordKeyMap.put(word, checkStringMach(editWord, ke.getValue()));
						}
					}
				}
			}
			if (!tmpWordKeyMap.containsKey(word)) {
				tmpWordKeyMap.put(word, e.getValue());
			}
		}
		return tmpWordKeyMap;
	}

	/**
	 * ワードとキーのマッチチェック
	 *
	 * @param word
	 * @param keyArray
	 * @return
	 */
	private String[] checkStringMach(String word, String[] keyArray) {
		List<String> keyLsit = new ArrayList<String>();
		String[] wordSplit = word.split(",", -1);
		for (String key : keyArray) {
			boolean isMach = true;
			String[] keySplit = key.split("", -1);
			for (int i = 0; i < wordSplit.length; i++) {
				if (wordSplit[i].matches(MATCH_WORD)) {
					continue;
				}
				if (!wordSplit[i].equals(keySplit[i + 1])) {
					isMach = false;
				}
			}
			if (isMach) {
				keyLsit.add(key);
			}
		}
		return keyLsit.toArray(new String[0]);
	}

	/**
	 * word要素とkey要素のマップ作成
	 *
	 * @param wordKeyMap
	 */
	private void setWorkElKeyElMap(Map<String, String[]> wordKeyMap) {
		for (Map.Entry<String, String[]> e : wordKeyMap.entrySet()) {
			String[] wElment = e.getKey().split(",", -1);
			String[] kElment = e.getValue()[0].split("", -1);
			for (int i = 0; i < wElment.length; i++) {
				wordElmentKeyElmentMap.put(wElment[i], kElment[i + 1]);
			}
		}
	}

	/**
	 * 旧配列値と新配列値からユニークな値のみで新しい配列を作る
	 *
	 * @param orizinalValue
	 * @param newValue
	 * @return ユニークな値のみ新配列
	 */
	private String[] editValue(String[] orizinalValue, String[] newValue) {
		List<String> tmpList = new ArrayList<String>();
		if (null == orizinalValue) {
			return newValue;
		}
		for (String oV : orizinalValue) {
			if (!tmpList.contains(oV)) {
				tmpList.add(oV);
			}
			for (String nV : newValue) {
				if (oV.equals(nV)) {
					continue;
				}
				if (!tmpList.contains(nV)) {
					tmpList.add(nV);
				}
			}
		}
		return tmpList.toArray(new String[0]);
	}

	/**
	 * 横ワードの要素を持つ縦ワード取得
	 *
	 * @param hword
	 * @return
	 */
	private List<String> getVWordListbyHWordElem(String hword) {
		List<String> tmpList = new ArrayList<String>();
		for (String hwordSplit : hword.split(",", -1)) {
			for (String vword : vWordArray) {
				if (hword.equals(vword)) {
					continue;
				}
				for (String vwordElm : vword.split(",", -1)) {
					if (vwordElm.equals(hwordSplit)) {
						tmpList.add(vword);
					}
				}
			}
		}
		return tmpList;
	}

	/**
	 * 縦ワードの要素を持つ横ワード取得
	 *
	 * @param hword
	 * @return
	 */
	private List<String> getHWordListbyVWordElem(String vword) {
		List<String> tmpList = new ArrayList<String>();
		for (String vwordSplit : vword.split(",", -1)) {
			for (String hword : hWordArray) {
				if (vword.equals(hword)) {
					continue;
				}
				for (String hwordElm : hword.split(",", -1)) {
					if (hwordElm.equals(vwordSplit)) {
						tmpList.add(hword);
					}
				}
			}
		}
		return tmpList;
	}

	/**
	 * 横ワードと縦ワードの共通文字位置のindexを取得
	 *
	 * @param hword
	 * @param vword
	 * @return 0:横ワードインデックス1:縦ワードインデックス
	 */
	private String[] getHVSameIndex(String hword, String vword) {
		String[] hwordArray = hword.split(",", -1);
		String[] vwordArray = vword.split(",", -1);
		for (int i = 0; i < hwordArray.length; i++) {
			if (!hwordArray[i].matches(MATCH_WORD)) {
				continue;
			}
			for (int j = 0; j < vwordArray.length; j++) {
				if (!vwordArray[j].matches(MATCH_WORD)) {
					continue;
				}
				if (hwordArray[i].equals(vwordArray[j])) {
					return new String[] { String.valueOf(i), String.valueOf(j) };
				}
			}
		}
		return null;
	}

	/**
	 * 答えを取得
	 *
	 * @return 答え
	 */
	public String getAnswer() {
		String answer = "";
		String word = cwField.getAnswerWord();
		for (String str : word.split(",", -1)) {
			if (null != str && !str.isEmpty()) {
				answer += wordElmentKeyElmentMap.get(str);
			}
		}
		return answer;
	}

	/**
	 * 答えを取得
	 *
	 * @return 答え
	 */
	private void displyFiled(String[][] filed) {
		for (int i = 0; i < filed.length; i++) {
			for (int j = 0; j < filed.length; j++) {
				System.out.print(filed[i][j]);
			}
			System.out.println();
		}
	}

	// ////////////////////////////////以下テスト用///////////////////////////////
	/**
	 * テスト用
	 *
	 * @param map マップ
	 */
	private void testDisply(String title, Map<String, String[]> map) {
		System.out.println(title);
		for (Map.Entry<String, String[]> e : map.entrySet()) {
			String str = "";
			for (String key : e.getValue()) {
				str += key + ",";
			}
			System.out.println("【word】" + e.getKey() + "【key】" + str);
		}
	}
}
