

/**
 * <pre>
 * crosswordのメインクラス.
 * </pre>
 *
 * @author y.ohsawa
 * @version 01.00.00
 */
public class CrossWord {

	/** Fieldクラスの宣言 */
	public CrossWordField cwField = null;

	/** Keyクラスの宣言 */
	public CrossWordKey cwKey = null;

	/** プロセスクラスの宣言 */
	public CrossWordCheckProc prc;

	/** keyチェックフラグ */
	public static boolean loopBreak = false;

	/**
	 * コンストラクタ
	 */
	public CrossWord() {
		cwField = new CrossWordField();
		cwKey = new CrossWordKey();
		prc = new CrossWordCheckProc(cwField, cwKey);
	}

	public static void main(String[] args) {
		CrossWord cw = new CrossWord();
		cw.prc.check();
		cw.prc.getAnswer();
		System.out.println("【答え】" + cw.prc.getAnswer());
	}
}
