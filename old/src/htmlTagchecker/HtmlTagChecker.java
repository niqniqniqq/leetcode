package htmlTagchecker;

import java.util.LinkedList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 *1.htmlを1行毎に読み込み、タグ部分を正規表現で抜き出す
 *2.抜き出したタグを順番に確認していく
 *	1.独立タグ<li>や無視してよいタグ<!DOCTYPE>等の場合、無視して次のタグのチェックへ移る。
 *	※無視するタグはIGNORE_ARRAYへ追加することで対応数を増やせる
 *	2.開始タグ<xx>である場合、開始タグリスト(startTagList)へ追加する。
 *	3.終了タグ</xx>である場合、開始タグリストに最後に追加されたタグとの一致を確認する。
 *	一致していれば入れ子となっているため、startTagListの最後の要素を取り除き処理を続行する。
 *	一致していなければ入れ子ではないため、処理中行数とチェックした終了タグを返す。
 *3.2の処理をすべての行に繰り返し、全タグを読み終わったら問題なしで正常終了。
 */

/**
 * <pre>
 * htmlタグチェッククラス
 * </pre>
 *
 * @author y.ohsawa
 * @version 01.00.00
 */
public class HtmlTagChecker {

	/** htmlタグ正規表現 */
	public static final String TAG_REG = "<.+?>";

	/** 終了タグ正規表現 */
	public static final String END_TAG_REG = "</.+?>";

	/** 無視するタグ文字(閉じてなくていもよい,独立してるタグを小文字文字列で記載) */
	public static final String[] IGNORE_ARRAY = new String[] { "meta", "br", "img", "hr", "input", "li", "p",
		"tr", "td", "th", "option", "!doctype", "head", "body", "html" };

	/** チェックするhtmlタグ */
	private List<String> htmlTag = null;

	/**
	 * コンストラクタ
	 *
	 * @param tag チェックするタグ
	 */
	HtmlTagChecker(List<String> tag) {
		htmlTag = tag;
	}

	/**
	 * htmlの誤り行を表示します。
	 */
	public void displayErrLineCnt() {
		String[] result = checkHtml();
		if (null == result) {
			System.out.println("正しいHTMLタグです");
		} else {
			System.out.println(result[0] + "行目の" + result[1] + "に誤りがあります");
		}
	}

	/**
	 * 閉じられていないタグと行数を返します。
	 *
	 * @return 0:誤行数 1:誤タグ名(正常の場合はnullが返ります)
	 */
	public String[] checkHtml() {
		List<String> startTagList = new LinkedList<String>();
		int lineCnt = 0;
		String extTag = null;
		String lastAddTag = null;
		Pattern p = null;
		Matcher m = null;
		for (String line : htmlTag) {
			lineCnt++;
			p = Pattern.compile(TAG_REG);
			m = p.matcher(line);
			while (m.find()) {
				extTag = m.group().trim();
				if (!checkIgnoreTag(extTag)) {
					if (!extTag.matches(END_TAG_REG)) {
						startTagList.add(getTagName(extTag));
					} else {
						lastAddTag = startTagList.get(startTagList.size() - 1);
						if (lastAddTag.equals(getTagName(extTag))) {
							startTagList.remove(startTagList.size() - 1);
						} else {
							return new String[] { String.valueOf(lineCnt), extTag };
						}
					}
				}
			}
		}
		return null;
	}

	/**
	 * 無視してよいタグか判別します
	 *
	 * @param tag 確認するタグ
	 * @return true:無視してよいタグ
	 */
	private boolean checkIgnoreTag(String tag) {
		String tagEl = getTagName(tag);
		for (String ig : IGNORE_ARRAY) {
			if (tagEl.toLowerCase().equals(ig) || //
				tagEl.toLowerCase().equals("/" + ig) || //
				tagEl.contains("!--")) {
				return true;
			}
		}
		return false;
	}

	/**
	 * タグ名を返します
	 *
	 * @param tag タグ
	 * @return タグ名※<div>→div:</div>→div
	 */
	private String getTagName(String tag) {
		String workStr = tag.substring(1, tag.length() - 1).trim().split(" ", -1)[0];
		if (workStr.startsWith("/")) {
			workStr = workStr.substring(1);
		}
		return workStr;
	}
}
