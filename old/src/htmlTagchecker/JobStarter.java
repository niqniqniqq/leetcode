package htmlTagchecker;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;



/**
 *<pre>
 * job実行クラス
 *</pre>
 *
 * @author y.ohsawa
 * @version 01.00.00
 */
public class JobStarter {

	/** ファイルパスリスト */
	private static List<String> filePathList = new LinkedList<String>();
	static {
		filePathList.add("src/htmlTagchecker/sample1.html");
		filePathList.add("src/htmlTagchecker/sample2.html");
	}

	/**
	 * メインメソッド
	 *
	 * @param arg コマンド引数
	 */
	public static void main(String[] arg) {
		HtmlTagChecker ht = null;
		for (String path : filePathList) {
			ht = new HtmlTagChecker(getFileContents(path));
			ht.displayErrLineCnt();
		}
	}

	/**
	 * 指定された通常ファイル内容をListで取得する.
	 *
	 * @param path ファイルパス＋ファイル名
	 * @param encode エンコード
	 * @return ファイル内容
	 */
	public static List<String> getFileContents(String path) {
		List<String> contentsList = new ArrayList<String>();
		FileInputStream inputStream = null;
		InputStreamReader reader = null;
		BufferedReader br = null;
		try {
			File f = new File(path);
			if (!f.exists() || !f.isFile()) {
				return Collections.emptyList();
			}
			inputStream = new FileInputStream(path);
			reader = new InputStreamReader(inputStream);
			br = new BufferedReader(reader);
			String line = null;
			while (null != (line = br.readLine())) {
				if (line.isEmpty()) {
					continue;
				}
				contentsList.add(line);
			}
		} catch (IOException e) {
			// throw new UserException(e, DHCM000005);
		} finally {
			if (null != inputStream) {
				try {
					inputStream.close();
				} catch (IOException e) {
					// 無視する
				}
			}
			if (null != reader) {
				try {
					reader.close();
				} catch (IOException e) {
					// 無視する
				}
			}
			if (null != br) {
				try {
					br.close();
				} catch (IOException e) {
					// 無視する
				}
			}
		}
		return contentsList;
	}
}
