import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * <pre>
 * キーを定義したクラス.
 * </pre>
 *
 * @author y.ohsawa
 * @version 01.00.00
 */
public class CrossWordKey {

	/** 縦キー */
	private String[] vKey;

	/** 横キー */
	private String[] hKey;

	/** 作業リスト */
	private List<String> workList = new ArrayList<String>();

	/**
	 * コンストラクタ
	 */
	public CrossWordKey() {
		setKey();
	}

	/**
	 * フィールド作成
	 */
	private void setKey() {
//		vKey = new String[] { "コオリ", "ニホンカミ", "モモ" };
//		hKey = new String[] { "ウニ", "オデン", "ホクロ", "アミモノ", "カモ" };
		// // //テスト用キー１
		// vKey = new String[] { "カオク", "ヒノデ", "リツトル", "ソン", "ツマズキ", "トピツク", "リヨウリ", "トクイ", "エンチヨウ" };
		// hKey = new String[] { "ソト", "トシ", "オリンピツク", "クツ", "イエ", "トツクリ", "ヒルマ", "ヨウチ", "ズノウ", "デンキ", "リソウ" };
		 //テスト用キー２
		 vKey =
		 new String[] { "キユウギヨウ", "ヒアタリ", "オカシイ", "リツトル", "ロンリテキ", "クイキ", "ギム", "ヨコ", "キユウカ", "トモダチ", "キチ",
		 "ウリキレ", "チンカ", "シヨウガ", "トイレツト", "タイメン", "イヤシ", "コカ", "ウシ", "レクリエーシヨン", "ゴザイマス", "トツキユウ" };
		 hKey =
		 new String[] { "キオク", "レキシ", "イチゴ", "ユカ", "チヨシヤ", "ウシロムキ", "シレイ", "ギイン", "ユウガタ", "クマ", "リヨウリ", "イギリス",
		 "ウリテ", "カキトメ", "ツキヨ", "レインコート", "ヒト", "コト", "カシツ", "アルク", "モチツキ", "ヨキ", "イチダント", "ウンユ", "リエキ",
		 "チカ", "ナシ" };
	}

	/**
	 * 全横キーワード取得
	 *
	 * @return 横キーワード
	 */
	public String[] getHKey() {
		return this.hKey;
	}

	/**
	 * 任意サイズの横キーワード取得
	 *
	 * @param size 文字列長
	 * @return 横キーワード
	 */
	public String[] getHKeyBySize(int size) {
		workList.clear();
		for (String str : hKey) {
			if (size == str.length()) {
				workList.add(str);
			}
		}
		return workList.toArray(new String[0]);
	}

	/**
	 * 全縦キーワード取得
	 *
	 * @return 縦キーワード
	 */
	public String[] getVKey() {
		return this.vKey;
	}

	/**
	 * 任意文字列サイズの縦キーワード取得
	 *
	 * @param size 文字列長
	 * @return 縦キーワード
	 */
	public String[] getVKeyBySize(int size) {
		workList.clear();
		for (String str : vKey) {
			if (size == str.length()) {
				workList.add(str);
			}
		}
		return workList.toArray(new String[0]);
	}

	/**
	 * 横ワード任意位置の文字と縦ワード任意位置の文字が等しい文字列を取得
	 *
	 * @param hId フィールド項番横ワードインデックス
	 * @param vId フィールド項番縦ワードインデックス
	 * @return Map<横キーワード配列、縦キーワード配列>
	 */
	public Map<String[], String[]> getHVKeyByIndex(int hId, int vId) {
		Map<String[], String[]> tmpMap = new HashMap<String[], String[]>();
		List<String> hKeyLsit = new ArrayList<String>();
		List<String> vKeyLsit = new ArrayList<String>();
		String indexHChar = null;
		String indexVChar = null;
		for (String h : hKey) {
			if (hId >= h.length()) {
				continue;
			}
			indexHChar = h.substring(hId, hId + 1);
			for (String v : vKey) {
				if (vId >= v.length()) {
					continue;
				}
				indexVChar = v.substring(vId, vId + 1);
				if (indexHChar.equals(indexVChar)) {
					if (!hKeyLsit.contains(h)) {
						hKeyLsit.add(h);
					}
					if (!vKeyLsit.contains(v)) {
						vKeyLsit.add(v);
					}
				}
			}
		}
		tmpMap.put(hKeyLsit.toArray(new String[0]), vKeyLsit.toArray(new String[0]));
		return tmpMap;
	}

	/**
	 * 横ワード任意位置の文字と縦ワード任意位置の文字が等しく、指定の文字列長を満たす文字列を取得
	 *
	 * @param hId フィールド項番横ワードインデックス
	 * @param hSize フィールド項番横ワード長
	 * @param vId フィールド項番縦ワードインデックス
	 * @param vSize フィールド項番縦ワード長
	 * @return Map<横キーワード配列、縦キーワード配列>
	 */
	public Map<String[], String[]> getHVKeyByIndexAndSize(int hId, int hSize, int vId, int vSize) {
		Map<String[], String[]> tmpMap = new HashMap<String[], String[]>();
		List<String> hKeyLsit = new ArrayList<String>();
		List<String> vKeyLsit = new ArrayList<String>();
		String indexHChar = null;
		String indexVChar = null;
		for (String h : hKey) {
			if (hSize != h.length()) {
				continue;
			}
			indexHChar = h.substring(hId, hId + 1);
			for (String v : vKey) {
				if (vSize != v.length()) {
					continue;
				}
				indexVChar = v.substring(vId, vId + 1);
				if (indexHChar.equals(indexVChar)) {
					if (!hKeyLsit.contains(h)) {
						hKeyLsit.add(h);
					}
					if (!vKeyLsit.contains(v)) {
						vKeyLsit.add(v);
					}
				}
			}
		}
		tmpMap.put(hKeyLsit.toArray(new String[0]), vKeyLsit.toArray(new String[0]));
		return tmpMap;
	}
}
