
package constant;

import java.util.HashMap;
import java.util.Map;

/**
 * <pre>
 * True/False.
 * </pre>
 *
 */
public enum TrueFalseEnum {
	/** 正常 */
	TRUE("true", "正常"),
	/** 異常 */
	FALSE("false", "異常"),
	/** イレギュラー */
	NONE("none", "");

	/**
	 * Enumのマップ
	 */
	private static Map<String, TrueFalseEnum> codeMap = new HashMap<String, TrueFalseEnum>();
	static {
		codeMap.put(TRUE.code, TRUE);
		codeMap.put(FALSE.code, FALSE);
		codeMap.put(TRUE.code.toUpperCase(), TRUE);
		codeMap.put(FALSE.code.toUpperCase(), FALSE);
	}

	/** コード */
	private String code = null;

	/** 名称 */
	private String name = null;

	/**
	 * コンストラクタ.
	 *
	 * @param code コード
	 * @param name 名称
	 */
	private TrueFalseEnum(String code, String name) {
		this.code = code;
		this.name = name;
	}

	/**
	 * コードからenumを取得する.
	 *
	 * @param code コード
	 * @return Enum
	 */
	public static TrueFalseEnum getEnumByCode(String code) {
		TrueFalseEnum e = codeMap.get(code);
		return e == null ? NONE : codeMap.get(code);
	}

	/**
	 * enum配列を取得する.
	 *
	 * @return enum配列
	 */
	public static TrueFalseEnum[] getEnumArray() {
		return codeMap.values().toArray(new TrueFalseEnum[0]);
	}

	/**
	 * コードを取得します.
	 *
	 * @return コード
	 */
	public String getCode() {
		return code;
	}

	/**
	 * 名称を取得します.
	 *
	 * @return 名称
	 */
	public String getName() {
		return name;
	}

	/**
	 * Trueか判定する.
	 *
	 * @return true:True
	 */
	public boolean isTrue() {
		return TRUE == this;
	}
}
