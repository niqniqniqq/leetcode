
package constant;

import java.util.HashMap;
import java.util.Map;

/**
 * <pre>
 * チェック結果.
 * </pre>
 *
 * @author y.yonekura
 * @version 01.00.00
 */
public enum CheckResultEnum {
	/** 正常 */
	VALID("valid", "正常"),
	/** 異常 */
	INVALID("invalid", "異常"),
	/** 警告 */
	WARNING("warning", "警告"),
	/** 未来日 */
	FUTURE("future", "未来日"),
	/** イレギュラー */
	NONE("none", "");

	/**
	 * Enumのマップ
	 */
	private static Map<String, CheckResultEnum> codeMap = new HashMap<String, CheckResultEnum>();
	static {
		codeMap.put(VALID.code, VALID);
		codeMap.put(INVALID.code, INVALID);
		codeMap.put(WARNING.code, WARNING);
		codeMap.put(FUTURE.code, FUTURE);
	}

	/** コード */
	private String code = null;

	/** 名称 */
	private String name = null;

	/**
	 * コンストラクタ.
	 *
	 * @param code コード
	 * @param name 名称
	 */
	private CheckResultEnum(String code, String name) {
		this.code = code;
		this.name = name;
	}

	/**
	 * コードからenumを取得する.
	 *
	 * @param code コード
	 * @return Enum
	 */
	public static CheckResultEnum getEnumByCode(String code) {
		CheckResultEnum e = codeMap.get(code);
		return e == null ? NONE : codeMap.get(code);
	}

	/**
	 * enum配列を取得する.
	 *
	 * @return enum配列
	 */
	public static CheckResultEnum[] getEnumArray() {
		return codeMap.values().toArray(new CheckResultEnum[0]);
	}

	/**
	 * コードを取得します.
	 *
	 * @return コード
	 */
	public String getCode() {
		return code;
	}

	/**
	 * 名称を取得します.
	 *
	 * @return 名称
	 */
	public String getName() {
		return name;
	}
}
