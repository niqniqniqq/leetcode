package constant;

import java.util.HashMap;
import java.util.Map;

import util.DateUtil;

public enum ColumnTypeEnum {
	/** 整数型 */
	INT("int", true, new String[] { "^[-|+]?[0-9]+$" }),
	/** 小数型 */
	FLOAT("float", true, new String[] { "^([-|+]?[0-9]+[¥.]?[0-9]*[f]?$)" }),
	/** 文字列型 */
	CHAR("char", true, new String[] {}),
	/** WORD型 */
	WORD("word", true, new String[] {}),
	/** CODE型 */
	CODE("code", true, new String[] {}),
	/** URL型 */
	// URL("url", "^(https?|ftp)(:\/\/[-_.!~*\'()a-zA-Z0-9;\/?:\@&=+\$,%#]+)$", DHVL000006),
	URL("url", true, new String[] { "" }),
	/** 日付時刻型 */
	DATETIME("datetime", false, new String[] { DateUtil.CHECK_DATE_TIME_FORMAT, DateUtil.CHECK_DATE_TIME_FORMAT2 }),
	/** 日付型 */
	DATE("date", false, new String[] { "^" + DateUtil.CHECK_DATE_FORMAT + "$",
		"^" + DateUtil.CHECK_DATE_FORMAT2 + "$", "^" + DateUtil.CHECK_DATE_FORMAT3 + "$" }),
	/** 日付型 */
	TIME("time", false, new String[] { "^" + DateUtil.CHECK_TIME_FORMAT + "$" }),
	/** イレギュラー */
	NONE("none", true, new String[] {});

	/**
	 * Enumのマップ
	 */
	private static Map<String, ColumnTypeEnum> codeMap = new HashMap<String, ColumnTypeEnum>();
	static {
		codeMap.put(INT.code, INT);
		codeMap.put(FLOAT.code, FLOAT);
		codeMap.put(CHAR.code, CHAR);
		codeMap.put(WORD.code, WORD);
		codeMap.put(CODE.code, CODE);
		codeMap.put(URL.code, URL);
		codeMap.put(DATETIME.code, DATETIME);
		codeMap.put(DATE.code, DATE);
		codeMap.put(TIME.code, TIME);
		codeMap.put(INT.code.toUpperCase(), INT);
		codeMap.put(FLOAT.code.toUpperCase(), FLOAT);
		codeMap.put(CHAR.code.toUpperCase(), CHAR);
		codeMap.put(WORD.code.toUpperCase(), WORD);
		codeMap.put(CODE.code.toUpperCase(), CODE);
		codeMap.put(URL.code.toUpperCase(), URL);
		codeMap.put(DATETIME.code.toUpperCase(), DATETIME);
		codeMap.put(DATE.code.toUpperCase(), DATE);
		codeMap.put(TIME.code.toUpperCase(), TIME);
	}

	/** コード */
	private String code = null;

	/** 名称 */
	private String[] name = null;

	/** 正規表現使用 */
	private boolean regularExpression = false;

	/**
	 * コンストラクタ.
	 *
	 * @param code コード
	 * @param regularExpression true:正規表現使用
	 * @param name 名称
	 * @param messageId エラー時メッセージID
	 */
	private ColumnTypeEnum(String code, boolean regularExpression, String name[]) {
		this.code = code;
		this.regularExpression = regularExpression;
		this.name = name;
	}

	/**
	 * コードからenumを取得する.
	 *
	 * @param code コード
	 * @return Enum
	 */
	public static ColumnTypeEnum getEnumByCode(String code) {
		ColumnTypeEnum e = codeMap.get(code);
		return e == null ? NONE : codeMap.get(code);
	}

	/**
	 * enum配列を取得する.
	 *
	 * @return enum配列
	 */
	public static ColumnTypeEnum[] getEnumArray() {
		return codeMap.values().toArray(new ColumnTypeEnum[0]);
	}

	/**
	 * コードを取得します.
	 *
	 * @return コード
	 */
	public String getCode() {
		return code;
	}

	/**
	 * 正規表現使用フラグを取得します.
	 *
	 * @return true:正規表現使用
	 */
	public boolean isRegularExpression() {
		return regularExpression;
	}

	/**
	 * 名称を取得します.
	 *
	 * @return 名称
	 */
	public String[] getName() {
		return name;
	}
}
