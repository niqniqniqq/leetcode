

import java.util.HashMap;
import java.util.Map;

/**
 * <pre>
 * crosswordのEnum.
 * </pre>
 *
 * @author y.ohsawa
 * @version 01.00.00
 */
public enum CrossWordEnum {
	/** 空白 */
	EMPTY("1", "□"),
	/** 無効空間 */
	INVILED("2", "■"),
	/** 異常 */
	NONE("", "");

	/**
	 * Enumのマップ
	 */
	private static Map<String, CrossWordEnum> codeMap = new HashMap<String, CrossWordEnum>();
	static {
		codeMap.put(EMPTY.code, EMPTY);
		codeMap.put(INVILED.code, INVILED);
	}

	/**
	 * Enumのマップ
	 */
	private static Map<String, CrossWordEnum> nameMap = new HashMap<String, CrossWordEnum>();
	static {
		nameMap.put(EMPTY.name, EMPTY);
		nameMap.put(INVILED.name, INVILED);
	}

	/** コード */
	private String code = null;

	/** 名称 */
	private String name = null;

	/**
	 * コンストラクタ.
	 *
	 * @param code
	 *            コード
	 * @param name
	 *            名称
	 */
	private CrossWordEnum(String code, String name) {
		this.code = code;
		this.name = name;
	}

	/**
	 * コードからenumを取得する.
	 *
	 * @param code
	 *            コード
	 * @return Enum
	 */
	public static CrossWordEnum getEnumByCode(String code) {
		CrossWordEnum e = codeMap.get(code);
		return e == null ? NONE : codeMap.get(code);
	}

	/**
	 * ネームからenumを取得する
	 *
	 * @param name
	 *            名称
	 * @return Enum
	 */
	public static CrossWordEnum getEnumByName(String name) {
		CrossWordEnum e = nameMap.get(name);
		return e == null ? NONE : nameMap.get(name);
	}

	/**
	 * コードを取得します.
	 *
	 * @return コード
	 */
	public String getCode() {
		return code;
	}

	/**
	 * 名称を取得します.
	 *
	 * @return 名称
	 */
	public String getName() {
		return name;
	}
}
